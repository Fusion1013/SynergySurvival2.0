package net.synergyserver.synergysurvival;

import net.synergyserver.synergycore.database.DataEntity;
import net.synergyserver.synergycore.database.DataManager;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Transient;

/**
 * Represents the data tracked for an item.
 */
@Entity(value = "itemdata")
public class ItemData implements DataEntity {

    @Id
    private ObjectId id;
    @Property("m")
    private String material;
    @Property("c")
    private long circulating;

    @Transient
    private DataManager dm = DataManager.getInstance();

    /**
     * Required constructor for Morphia to work.
     */
    public ItemData() {}

    /**
     * Creates a new <code>ItemData</code> with the given parameters.
     *
     * @param material The name of the material that this <code>ItemData</code> represents.
     */
    public ItemData(String material) {
        this.id = new ObjectId();

        this.material = material;
    }

    @Override
    public ObjectId getID() {
        return id;
    }

    /**
     * Gets the name of the material that this <code>ItemData</code> represents.
     *
     * @return The name of the material.
     */
    public String getMaterial() {
        return material;
    }

    /**
     * Gets the key that is used to refer to this <code>ItemData</code>'s item.
     *
     * @return The key of this <code>ItemData</code>.
     */
    public String getKey() {
        return material;
    }

    /**
     * Gets the number of items that this <code>ItemData</code> represents that are currently circulating.
     *
     * @return The number of circulating items.
     */
    public long getCirculating() {
        return circulating;
    }

    /**
     * Changes the number of circulating items of the item that this <code>ItemData</code> represents.
     *
     * @param change The amount to change the number of circulating items by.
     */
    public void changeCirculating(long change) {
        this.circulating += change;
        dm.updateField(this, ItemData.class, "c", circulating);
    }
}
