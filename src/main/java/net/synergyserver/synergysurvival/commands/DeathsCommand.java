package net.synergyserver.synergysurvival.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "deaths",
        aliases = "death",
        permission = "syn.deaths",
        usage = "/deaths <check|top>",
        description = "Main command for checking deaths.",
        minArgs = 1
)
public class DeathsCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
