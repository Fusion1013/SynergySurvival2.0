package net.synergyserver.synergysurvival.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergysurvival.SurvivalWorldGroupProfile;
import net.synergyserver.synergysurvival.SynergySurvival;
import org.bukkit.command.CommandSender;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.aggregation.Projection;
import org.mongodb.morphia.aggregation.Sort;

import java.util.Iterator;
import java.util.LinkedHashMap;

@CommandDeclaration(
        commandName = "top",
        aliases = "leaderboard",
        permission = "syn.deaths.top",
        usage = "/deaths top",
        description = "Displays the top 10 people in number of deaths in the Survival world group.",
        maxArgs = 0,
        parentCommandName = "deaths"
)
public class DeathsTopCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Datastore ds = MongoDB.getInstance().getDatastore();

        // Get the top 10 WorldGroupProfiles
        Iterator<SurvivalWorldGroupProfile> wgps = ds.createAggregation(WorldGroupProfile.class)
                .match(
                        ds.find(WorldGroupProfile.class, "n =", SynergySurvival.getWorldGroupName())
                )
                .project(
                        Projection.projection("pid"),
                        Projection.projection("d")
                )
                .sort(
                        Sort.descending("d")
                )
                .limit(10)
                .aggregate(SurvivalWorldGroupProfile.class, MongoDB.getAggregationOptions());


        LinkedHashMap<String, Integer> deaths = new LinkedHashMap<>();

        // Store the results
        while (wgps.hasNext()) {
            SurvivalWorldGroupProfile wgp = wgps.next();
            deaths.put(PlayerUtil.getName(wgp.getPlayerID()), wgp.getDeaths());
        }

        // Send the header
        sender.sendMessage(Message.format("commands.deaths.top.header"));

        // Send the top 10
        int i = 1;
        for (String name : deaths.keySet()) {
            sender.sendMessage(Message.format("commands.deaths.top.item", i, name, deaths.get(name)));

            // Increment the index
            i++;
        }
        return true;
    }
}
