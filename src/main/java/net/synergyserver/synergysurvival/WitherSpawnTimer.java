package net.synergyserver.synergysurvival;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;


public class WitherSpawnTimer extends BukkitRunnable {

    private Entity witherEntity;

    public WitherSpawnTimer(Entity wither) {
        this.witherEntity = wither;
    }

    @Override
    public void run() {
        if (!this.witherEntity.isValid()) {
            this.cancel();
        } else {
            for (int i = 0; i < MathUtil.randInt(1, 3); i++) {
                // Spawn a wither skeleton at the skull's location
                WitherSkeleton skeleton = witherEntity.getLocation().getWorld().spawn(witherEntity.getLocation(), WitherSkeleton.class);
                EntityEquipment equipment = skeleton.getEquipment();

                // Give it an enchanted sword
                ItemStack sword = new ItemStack(Material.STONE_SWORD);
                sword.addEnchantment(Enchantment.DAMAGE_ALL, 5);
                sword.addEnchantment(Enchantment.FIRE_ASPECT, 2);
                equipment.setItemInMainHand(sword);

                // Give it speed and strength
                skeleton.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));
                skeleton.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));

                // Make it so its equipment can't be farmed
                skeleton.setCanPickupItems(false);
                skeleton.setMetadata("noDrops", new FixedMetadataValue(SynergySurvival.getPlugin(), true));

                // Kill the wither skeleton after 2 minutes has passed
                Bukkit.getScheduler().runTaskLater(SynergyCore.getPlugin(), () -> {
                    if (!skeleton.isDead()) {
                        skeleton.remove();
                    }
                }, TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MINUTE, TimeUtil.TimeUnit.GAME_TICK, 2));
            }
        }
    }
}
