package net.synergyserver.synergysurvival.listeners;

import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import net.synergyserver.synergysurvival.SynergySurvival;
import net.synergyserver.synergysurvival.WitherExplodeTimer;
import net.synergyserver.synergysurvival.WitherSpawnTimer;
import net.synergyserver.synergysurvival.projectiles.SeekingFireball;
import net.synergyserver.synergysurvival.settings.SurvivalTieredMultiOptionSetting;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.type.Snow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Endermite;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Rabbit;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Wither;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * Listens to events that are caused by entities.
 */
public class EntityListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMobSpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }
        switch (entity.getType()) {
            case ENDER_DRAGON:
                // If it's the ender dragon, check the config value
                if (!PluginConfig.getConfig(SynergySurvival.getPlugin()).getBoolean("enable_dragon")) {
                    event.setCancelled(true);
                }
                break;
            case WITHER:
                // If it's a wither, start spawning packs of wither skeletons every 15 seconds.
                new WitherSpawnTimer(entity).runTaskTimer(SynergySurvival.getPlugin(),
                        0L,
                        TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.SECOND, TimeUtil.TimeUnit.GAME_TICK, 15));

                // Also wait 0 seconds then start checking for nearby entities and exploding if they are there every 10 seconds
                new WitherExplodeTimer(entity).runTaskTimer(SynergySurvival.getPlugin(),
                        0L,
                        TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.SECOND,TimeUtil.TimeUnit.GAME_TICK, 10));
                break;
            // If it's a rabbit then have a 1/20 chance of it being a killer rabbit
            case RABBIT:
                if (MathUtil.randChance(0.05)) {
                    ((Rabbit) entity).setRabbitType(Rabbit.Type.THE_KILLER_BUNNY);
                }
                break;
            // If it's a zombie then have a 1/8 chance of spawning in a larger pack
            case ZOMBIE:
                if (MathUtil.randChance(0.125)) {
                    // Spawn up to 3 additional zombies
                    for (int i = 0; i < MathUtil.randInt(1, 3); i++) {
                        entity.getWorld().spawn(entity.getLocation(), Zombie.class);
                    }
                }
            // If it's a bipedal monster then allow it to always pick up items
            case SKELETON:
            case VINDICATOR:
            case PIG_ZOMBIE:
                entity.setCanPickupItems(true);
                break;
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onMobTarget(EntityTargetEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // Ignore if it's not an entity we care about
        if (entity instanceof WitherSkeleton) {
            // Check if it's a wither skeleton spawned by the wither boss
            if (!entity.hasMetadata("noDrops")) {
                return;
            }
        } else if (entity instanceof Rabbit) {
            // Check if it's a killer rabbit
            if (!((Rabbit) entity).getRabbitType().equals(Rabbit.Type.THE_KILLER_BUNNY)) {
                return;
            }
        } else {
            return;
        }

        // Ignore if it's not a player
        if (!(event.getTarget() instanceof Player)) {
            return;
        }

        // Ignore if it's not the skeleton targeting
        switch (event.getReason()) {
            case CUSTOM:
            case FORGOT_TARGET:
            case TARGET_DIED:
            case UNKNOWN:
                return;
            default: break;
        }

        // Cancel the event if the target is a player with weak difficulty
        if (!SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(PlayerUtil.getProfile((Player) event.getTarget()), "extremely_easy")) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMobDamage(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // Attempt to get the damaging player
        Entity damager = event.getDamager();
        Player damagingPlayer = null;
        if (damager instanceof Player) {
            damagingPlayer = (Player) damager;
        } else if (damager instanceof Arrow && ((Arrow) damager).getShooter() instanceof Player) {
            damagingPlayer = (Player) ((Arrow) damager).getShooter();
        }

        // If it's not a player doing damage ignore the event
        if (damagingPlayer == null) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(damagingPlayer);

        if (entity instanceof PigZombie) {
            // If the player hit a zombie pigmen then have a 1/16 chance of spawning a new one

            // Only care if the target is a player with a higher difficulty
            if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "extremely_easy")) {
                // If they're farming pig zombies then don't spawn a new one
                Damageable damageable = (Damageable) entity;
                if (damageable.getHealth() > 3) {
                    if (MathUtil.randChance(.0625)) {
                        PigZombie pigZombie = entity.getWorld().spawn(entity.getLocation(), PigZombie.class);
                        // Make it agro
                        pigZombie.setAngry(true);
                        // Flag this pig to not drop items
                        pigZombie.setMetadata("noDrops", new FixedMetadataValue(SynergySurvival.getPlugin(), true));
                    }
                }
            }
        } else if (entity instanceof Enderman) {
            // If they hit an enderman then have a 1/8 chance of spawning an endermite

            // Only care if the target is a player with a higher difficulty
            if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "extremely_easy")) {
                if (MathUtil.randChance(0.125)) {
                    entity.getWorld().spawn(entity.getLocation(), Endermite.class);
                }
            }
        } else if (entity instanceof Wither) {
            if (damager instanceof Player) {
                // If player hits a wither with a stick, kill the wither instantly and drop 3 wither skulls
                Player player = (Player) damager;
                if (player.getInventory().getItemInMainHand().getType().equals(Material.STICK)) {
                    entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.WITHER_SKELETON_SKULL, 3));
                    entity.remove();
                }
            }
        } else if (entity instanceof SeekingFireball) {
            // If it was a SeekingFireball then set it to not be seeking
            SeekingFireball fireball = (SeekingFireball) entity;
            fireball.setSeeking(false);
        }

        // If the player has a high enough difficulty then return half the damage done back to them
        if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "pretty_easy")) {
            // Calculate the damage that would take the player to their last half heart
            double toLastHalfHeart = damagingPlayer.getHealth() - 1D;
            // Damage the player half the amount they did, to a maximum of half a heart remaining
            damagingPlayer.damage(Math.min(event.getFinalDamage() / 2, toLastHalfHeart));
        }

        // If it was a flaming arrow that did damage then do spread damage
        if (!(damager instanceof Arrow)) {
            return;
        }

        Arrow arrow = (Arrow) damager;
        if (arrow.getFireTicks() <= 0) {
            return;
        }

        for (Entity e : arrow.getNearbyEntities(1, 1, 1)) {
            if (e.getFireTicks() < 60 && !e.equals(arrow.getShooter())) {
                e.setFireTicks(60);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMobDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // Make withers immune to explosions
        if (entity instanceof Wither && (event.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_EXPLOSION) ||
                event.getCause().equals(EntityDamageEvent.DamageCause.BLOCK_EXPLOSION))) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMobDeath(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // If it was a spider that was killed by a player then have a 1/8 chance of spawning cave spiders
        // Only care if the target is a player with a higher difficulty
        if (event.getEntity().getKiller() != null && entity.getType().equals(EntityType.SPIDER)) {
            if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(PlayerUtil.getProfile(event.getEntity().getKiller()), "extremely_easy")) {
                if (MathUtil.randChance(0.125)) {
                    // Spawn up to 5 cave spiders
                    for (int i = 0; i < MathUtil.randInt(1, 5); i++) {
                        entity.getWorld().spawn(entity.getLocation(), CaveSpider.class);
                    }
                }
            }
        }

        // If the entity has drops disabled then don't drop items or xp
        if (entity.hasMetadata("noDrops")) {
            event.setDroppedExp(0);

            // Drop any picked up items on the ground before clearing the rest
            EntityEquipment equipment = entity.getEquipment();
            if (equipment.getHelmetDropChance() > 2) {
                entity.getWorld().dropItemNaturally(entity.getLocation(), equipment.getHelmet());
            }
            if (equipment.getChestplateDropChance() > 2) {
                entity.getWorld().dropItemNaturally(entity.getLocation(), equipment.getChestplate());
            }
            if (equipment.getLeggingsDropChance() > 2) {
                entity.getWorld().dropItemNaturally(entity.getLocation(), equipment.getLeggings());
            }
            if (equipment.getBootsDropChance() > 2) {
                entity.getWorld().dropItemNaturally(entity.getLocation(), equipment.getBoots());
            }
            if (equipment.getItemInMainHandDropChance() > 2) {
                entity.getWorld().dropItemNaturally(entity.getLocation(), equipment.getItemInMainHand());
            }
            if (equipment.getItemInOffHandDropChance() > 2) {
                entity.getWorld().dropItemNaturally(entity.getLocation(), equipment.getItemInOffHand());
            }
            event.getDrops().clear();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onGhastShoot(ProjectileLaunchEvent event) {
        Projectile projectile = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(projectile.getWorld().getName())) {
            return;
        }

        // Ignore the event if the shooter wasn't a ghast
        if (projectile.getShooter() == null || !(projectile.getShooter() instanceof Ghast)) {
            return;
        }

        Ghast ghast = (Ghast) projectile.getShooter();

        // Ignore the event if, for some reason, it wasn't a fireball that was shot
        if (!(projectile instanceof Fireball)) {
            return;
        }

        // Ignore the event if the fireball is already custom
        if (projectile instanceof SeekingFireball) {
            return;
        }

        Fireball fireball = (Fireball) projectile;

        // Kill the fireball and spawn a custom, player-seeking one
        SeekingFireball seekingFireball = new SeekingFireball(fireball.getLocation(), ghast);
        seekingFireball.setDirection(fireball.getDirection());
        seekingFireball.setVelocity(fireball.getDirection());
        fireball.remove();
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onCreeperPrime(ExplosionPrimeEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        if (entity instanceof EnderCrystal) {
            // If it's an end crystal, check the config value for if the ender dragon battle should be allowed or not
            if (!PluginConfig.getConfig(SynergySurvival.getPlugin()).getBoolean("enable_dragon")) {
                event.setCancelled(true);
            }
        } else if (entity instanceof Creeper) {
            Creeper creeper = (Creeper) entity;
            LivingEntity target = creeper.getTarget();
            if (target instanceof Player) {
                Player targetedPlayer = (Player) target;

                // Only care if the target is a player with a higher difficulty
                if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(PlayerUtil.getProfile(targetedPlayer), "extremely_easy")) {
                    float healthiness = (float) creeper.getHealth() / 20;

                    if (creeper.isPowered()) {
                        // If the creeper is powered then make it set fire to things and increase its radius by a lot
                        event.setFire(true);
                        float maxRadius = event.getRadius() + 4;
                        event.setRadius(maxRadius * healthiness);
                        creeper.setMetadata("explosionRadius", new FixedMetadataValue(SynergySurvival.getPlugin(), event.getRadius()));
                    } else {
                        // Otherwise just increase its radius
                        float maxRadius = event.getRadius() + 2;
                        event.setRadius(maxRadius * healthiness);
                        creeper.setMetadata("explosionRadius", new FixedMetadataValue(SynergySurvival.getPlugin(), event.getRadius()));
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSnowballHit(ProjectileHitEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        Block block = event.getHitBlock();

        // If the entity isn't a snowball or if it didn't hit a block then ignore it.
        if (!(entity instanceof Snowball) || block == null) {
            return;
        }

        Location ballLoc = entity.getLocation();

        boolean directlyOnTop = ballLoc.getBlockX() == block.getX() && ballLoc.getBlockY() == block.getY() && ballLoc.getBlockZ() == block.getZ();
        boolean aroundTheTop = ballLoc.getY() > block.getY();

        // If it didn't hit the top of the block then ignore it
        if (!directlyOnTop && !aroundTheTop) {
            return;
        }

        // If the block the snowball hit is already snow then increase the height
        if (block.getBlockData() instanceof Snow) {
            Snow snow = (Snow) block.getBlockData();
            // If it's the highest height then just set it to a full snow block. If it's a full block already then ignore it
            if (snow.getLayers() == snow.getMaximumLayers()) {
                block.setType(Material.SNOW_BLOCK);
                block.getState().update();
            } else if (snow.getLayers() < snow.getMaximumLayers()) {
                // Otherwise increase the height of the snow by one
                snow.setLayers(snow.getLayers() + 1);
                block.getState().update();
            }
        } else {
            // If the block above is air then set it to snow instead
            Location locationAbove = block.getLocation().add(0, 1, 0);
            Block blockAbove = locationAbove.getBlock();

            if (ItemUtil.isAir(blockAbove.getType())) {
                blockAbove.setType(Material.SNOW);
                blockAbove.getState().update();
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onEndermanChangeBlock(EntityChangeBlockEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // If the entity is an enderman trying to change a block, stop it
        if (entity.getType() == EntityType.ENDERMAN){
            event.setCancelled(true);
        }
    }
}
