package net.synergyserver.synergysurvival.projectiles;

import net.coreprotect.CoreProtect;
import net.coreprotect.CoreProtectAPI;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergysurvival.SynergySurvival;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Represents a brick projectile. Bricks are heavy, lobbed projectiles that do large damage and have potion effects.
 */
public class BrickProjectileBase extends ItemProjectile {

    public BrickProjectileBase(Location location, Player shooter) {
        super(location, shooter, new ItemStack(Material.BRICK));

        // Play a sound where the projectile was shot
        location.getWorld().playSound(location, Sound.ENTITY_ARROW_SHOOT, 1, -8);
    }

    @Override
    public void tick() {
        super.tick();

        if (dead) {
            return;
        }

        // Brick movement
        setMot(getMot().add(0, -0.1, 0));

        // If this is on the ground, allow it to be picked up and don't have glitchy movement
        if (onGround) {
            pickupDelay = 0;
            setMot(0, 0, 0);
        }
    }

    @Override
    public int getLife() {
        return Integer.MAX_VALUE;
    }

    @Override
    public double getSpeed() {
        return .75;
    }

    @Override
    public void hitEntity(Damageable entity) {
        // If the entity is dying then ignore it
        if (entity.getHealth() <= 0) {
            return;
        }

        // Call the parent method and damage the entity
        super.hitEntity(entity);
        entity.damage(4);

        // If the damaged entity is a player then give them confusion and blindness
        if (entity instanceof Player) {
            Player player = (Player) entity;

            player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 120, 127));
            player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 1));

            // Set the damage cause to be custom so a generic death message doesn't broadcast
            EntityDamageEvent event = new EntityDamageEvent(player, EntityDamageEvent.DamageCause.CUSTOM, 4);
            player.setLastDamageCause(event);

            // If they died then broadcast a custom message
            if (player.isDead()) {
                PlayerUtil.killPlayer(player, "death_message.brick", getShooter());
            }
        }
    }

    @Override
    public void hitBlock(Block block) {
        // If the block can be broken by bricks then do it
        YamlConfiguration config = PluginConfig.getConfig(SynergySurvival.getPlugin());
        if (config.getList("brick_breakable").contains(block.getType().name())) {
            // Log the removal
            CoreProtectAPI cp = ((CoreProtect) Bukkit.getPluginManager().getPlugin("CoreProtect")).getAPI();
            cp.logRemoval(getShooter().getName(), block.getLocation(), block.getType(), block.getBlockData());

            block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, block.getType(), 5);
            block.breakNaturally();
        }
    }
}
