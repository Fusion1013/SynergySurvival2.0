package net.synergyserver.synergysurvival.projectiles;

import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Represents a flint projectile. Flint are rapid-fire projectiles that do small damage.
 */
public class FlintProjectileBase extends ItemProjectile {

    public FlintProjectileBase(Location location, Player shooter) {
        super(location, shooter, new ItemStack(Material.FLINT));

        // Play a sound where the projectile was shot
        location.getWorld().playSound(location, Sound.ENTITY_ARROW_SHOOT, 1, 8);
    }

    @Override
    public void tick() {
        super.tick();

        if (dead) {
            return;
        }

        // Create trailing particles
        world.getWorld().spawnParticle(Particle.CRIT_MAGIC, getLocation(), 4);
    }

    @Override
    public double getSpeed() {
        return 4;
    }

    @Override
    public void hitEntity(Damageable entity) {
        // If the entity is dying then ignore it
        if (entity.getHealth() <= 0) {
            return;
        }

        // Call the parent method and damage the entity
        super.hitEntity(entity);
        entity.damage(2);

        if (entity instanceof Player) {
            Player player = (Player) entity;

            // Set the damage cause to be custom so a generic death message doesn't broadcast
            EntityDamageEvent event = new EntityDamageEvent(player, EntityDamageEvent.DamageCause.CUSTOM, 4);
            player.setLastDamageCause(event);

            // If they died then broadcast a custom message
            if (player.isDead()) {
                PlayerUtil.killPlayer(player, "death_message.flint", getShooter());
            }
        }
    }

    @Override
    public void hitBlock(Block block) {
        // Call the parent method and create impact particles
        super.hitBlock(block);
        world.getWorld().spawnParticle(Particle.ITEM_CRACK, getLocation(), 4, new ItemStack(Material.FLINT));
    }
}
